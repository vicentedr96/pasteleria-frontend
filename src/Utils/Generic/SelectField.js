import React, { memo } from 'react';
import { makeStyles, FormControl, InputLabel } from "../Constant/core";
const useStyles = makeStyles({
    formControl: {
        minWidth: 120,
    },
});
const FieldCmp = memo((props) => {
    let { children, title } = props;
    const classes = useStyles();
    return (
        <FormControl variant="outlined" className={classes.formControl} style={{ width: "100%" }}>
            <InputLabel htmlFor="outlined-age-native-simple--01">{title}</InputLabel>
            {children}
        </FormControl>
    );
})

export default FieldCmp;