import React, { Suspense, lazy } from 'react';
import { Routes, Route, BrowserRouter } from "react-router-dom";
import { getTheme } from "./Settings/index";
import { MuiThemeProvider } from "@material-ui/core/styles";
import Progress from "./Utils/Generic/Progress";

const Pagina = lazy(()=> import("./Views/index"));

function App() {
  return (
    <>
      <MuiThemeProvider theme={getTheme()}>
        <BrowserRouter>
        <Suspense fallback={<Progress />}>
          <Routes >
            <Route path="/" element={<Pagina />} />
          </Routes>
          </Suspense>
        </BrowserRouter>
      </MuiThemeProvider>
    </>
  );
}

export default App;
