import { createTheme } from '@material-ui/core/styles';
import Mulish from "../assets/fonts/Mulish.ttf";

//TIPOGRAFIAS
const MULISH = {
  fontFamily: 'Mulish',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    url(${Mulish}) format('ttf')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

//PALETA DE COLORES
const LAVANDA = {
  light: '#B39CD0',
  main: '#845EC2',
  dark: '#492986',
  contrastText: '#fff',
};
const SECONDARY= {
  light: '#ff7961',
  main: '#f44336',
  dark: '#ba000d',
  contrastText: '#000',
};
const ERROR= {
  light: '#ff7961',
  main: '#f44336',
  dark: '#ba000d',
  contrastText: '#000',
};

export const getTheme = () => {

  let color = LAVANDA;
  return createTheme({
    typography: {
      fontFamily: 'Mulish',
    },
    overrides: {
      MuiInput: {
        underline: {
          '&:before': {
            borderBottom: `2px solid ${LAVANDA.main}`
          },
          '&:after': {
            borderBottom: `2px solid ${LAVANDA.main}`
          }
        }
      },
      MuiCssBaseline: {
        '@global': {
          '@font-face': [MULISH],
        },
      },
    },
    palette: {
      primary: color,
      secondary: SECONDARY,
      error: ERROR
    },
  })

}

//FUNCION DE COLORES GLOBALES
export const colorPalettes = (theme) => ({
  primaryColor: {
    color: theme.palette.primary.main
  },
  grayLetter: {
    color: "#616A77!important"
  },
  cloudBlueColor: {
    color: `#689AEB!important`
  },
  redColor: {
    color: `#F5365C!important`
  },
  orangeColor: {
    color: `#FB6340!important`
  },
  yellowColor: {
    color: `#FFD600!important`
  },
  lightBlueColor: {
    color: `#11CFF2!important`
  },
  pinkColor: {
    color: `#F3A4B5!important`
  },
  purpleColor: {
    color: `#ab47bc!important`
  },
  greenColor: {
    color: `#2e7d32!important`
  },
  greyColor: {
    color: `#616A77!important`
  },
  metallicGrayColor: {
    color: `#4B4453!important`
  },
  lilacColor: {
    color: `#845EC2!important`
  },
  mintColor: {
    color: `#00C9A7!important`
  },
  emeraldColor: {
    color: `#008F7A!important`
  },
  metallicGrayBg: {
    background: `#4B4453!important`
  },
  emeraldBg: {
    background: `#008F7A!important`
  },
  mintBg: {
    background: `#00C9A7!important`
  },
  lilacBg: {
    backgroundColor: `#845EC2!important`
  },
  greyBg: {
    backgroundColor: `#616A77!important`
  },
  greenBg: {
    backgroundColor: `#2e7d32!important`
  },
  cloudBlueBg: {
    backgroundColor: `#689AEB!important`
  },
  redBg: {
    backgroundColor: `#F5365C!important`
  },
  orangeBg: {
    backgroundColor: `#FB6340!important`
  },
  yellowBg: {
    backgroundColor: `#FFD600!important`
  },
  lightBlueBg: {
    backgroundColor: `#11CFF2!important`
  },
  pinkBg: {
    backgroundColor: `#F3A4B5!important`
  },
  purpleBg: {
    backgroundColor: `#ab47bc!important`
  },
})

