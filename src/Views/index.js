import BarraSuperior from "../Components/BarraSuperior";
import Menu from "../Components/Menu";
import Cuerpo from "../Components/Cuerpo";
export default function Pagina() {
    return (
        <div>
            <BarraSuperior />
            <Menu />
            <Cuerpo />
        </div>
    )
}
