//***CORE***
import { Grid, Typography, makeStyles } from '../Utils/Constant/core';

const useStyles = makeStyles(theme => ({
    alinearBarraSuperior: {
        [theme.breakpoints.up('xs')]: {
            justifyContent: 'flex-end',
        },
        [theme.breakpoints.up('sm')]: {
            justifyContent: 'flex-end',
        }
    },
    alinearBlog:{
        textAlign:"right",
        [theme.breakpoints.down('sm')]: {
            textAlign:"center",
        }
    },
    alinearContacto:{
        textAlign:"center",
        [theme.breakpoints.down('sm')]: {
            textAlign:"center",
        }
    },
    alinearEn:{
        [theme.breakpoints.down('sm')]: {
            textAlign:"center",
        }
    }
}));

export default function BarraSuperior() {
    const classes = useStyles();
    return (
            <Grid container item xs={12} className="Rectangle-1">
                <Grid item xs={12} sm={6}
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                >
                <Grid item xs={12}  className={classes.alinearEn}>
                    <Typography variant="subtitle1" component="h2">
                    <span className="ENG">ENG |</span>
                    </Typography>
                </Grid>
                </Grid>
                <Grid item xs={12} sm={6}
                    container
                    direction="row"
                    alignItems="center"
                    justifyContent='flex-end'
                    className={classes.alinearBarraSuperior}
                >
             
                <Grid item xs={12} sm={4} className={classes.alinearBlog}>
                <Typography variant="subtitle1" component="h2">
                    <span className="Blog">BLOG</span>
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={4} className={classes.alinearContacto}>
                <Typography variant="subtitle1" component="h2">
                    <span className="Contacto">CONTACTO</span>
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={2} />
                </Grid>
            </Grid>
    );
}
