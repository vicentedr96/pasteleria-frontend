import { Grid, Typography, makeStyles, Hidden } from '../Utils/Constant/core';
import img from "../assets/img/mi-pasteleria.svg";

const useStyles = makeStyles(theme => ({
    alinearMenu: {
        [theme.breakpoints.down('md')]: {
            textAlign: "center!important",
            justifyContent:"center"
        },
    },
    
        alinearM:{
            [theme.breakpoints.down('md')]: {
                justifyContent:"center"
            },
        }
    
}));

export default function Menu() {
    const classes = useStyles();
    return (
        <Grid  item xs={12} className="Rectangle-2"
            container
            direction="row"
            justifyContent="space-around"
            alignItems="center"
        >
            <Hidden xsDown>
                <Grid item xs={12} sm={6} md={4} lg={4}>
                    <img src={img} className="Mi-Pasteleria" alt="comida" />
                </Grid>
            </Hidden>
            <Grid item xs={12} lg={6} container spacing={1} className={classes.alinearMenu}>
                <Grid item xs={12} md={2} >
                    <Typography variant="subtitle1" component="span" gutterBottom className="Pasteles">
                        Pasteles
                    </Typography>
                </Grid>
                <Grid item xs={12} md={2}>
                    <Typography variant="subtitle1" component="span" gutterBottom className="Helados">
                        Helados
                    </Typography>
                </Grid>
                <Grid item xs={12} md={2}>
                    <Typography variant="subtitle1" component="span" gutterBottom className="Galletas">
                        Galletas
                    </Typography>
                </Grid>
                <Grid item xs={12} md={2}>
                    <Typography variant="subtitle1" component="span" gutterBottom className="Nosotros">
                        Nosotros
                    </Typography>
                </Grid>
                <Grid item xs={12} md={2}>
                    <Typography variant="subtitle1" component="span" gutterBottom className="Sucursales">
                        Sucursales
                    </Typography>
                </Grid>
            </Grid>
        </Grid>
    )
}
