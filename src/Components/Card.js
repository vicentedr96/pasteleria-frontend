import { Grid, Typography, Button, Hidden, makeStyles } from '../Utils/Constant/core';

const useStyles = makeStyles(theme => ({
    alinear: {
        padding: "10px", margin: "0px",
        [theme.breakpoints.down('md')]: {
            textAlign: "center!important",
        },
    },
    fontBold: {
        fontWeight: "bold"
    },
    mgt4: {
        marginTop: "4px"
    }
}));

export default function Card(props) {
    const classes = useStyles();
    let { name, imageUrl, description, price, comboPrice, toppingType, size } = props;
    return (
        <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
        >
            <div className="Rectangle-card ">
                <Grid
                    container
                    direction="row"
                    justifyContent="flex-end"
                    alignItems="center"
                    className="borderBottom"
                >
                    <Grid item xs={12} md={12} lg={4}>
                        <Typography variant="subtitle1" component="p" gutterBottom className="Pastel-de-Moka">
                            {name}
                        </Typography>
                    </Grid>
                    <Hidden mdDown>
                        <Grid item xs={8}
                            container
                            direction="row"
                            alignItems="center"
                            justifyContent='flex-end'
                        >
                            <div className="Rectangle-title-1">
                                <span className="PASTEL">
                                    PASTEL
                                </span>
                            </div>
                            <div className="Rectangle-title-2">
                                <span className="PASTEL">
                                    PAQUETE
                                </span>
                            </div>
                        </Grid>
                    </Hidden>
                </Grid>

                <Grid item xs={12}
                    container
                    direction="row"
                    alignItems="center"
                    justifyContent='center'
                >
                    <Grid item xs={12} md={3} style={{ textAlign: "center" }}>
                        <img src={imageUrl} className="Bitmap" alt="comida"/>
                    </Grid>
                    <Grid item xs={12} md={4} container direction="column">
                        <p style={{ padding: "10px" }}>{description}</p>
                        <p className={classes.alinear} ><span className={classes.fontBold}>Tamaño: </span>{size}</p>
                        <p className={classes.alinear} ><span className={classes.fontBold}>Topping: </span>{toppingType}</p>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2}
                        container
                        direction="column"
                        alignItems="center"
                        justifyContent='center'>
                        <span className="---17-Personas">
                            15 - 17 Personas
                        </span>
                        <span className="span-1">
                            <span className="text-style-1">$</span>{price}
                        </span>
                        <span className="Slo-pastel">
                            *Sólo pastel
                        </span>
                        <Button variant="contained" color="primary" className={classes.mgt4}>
                            Seleccionar
                        </Button>
                    </Grid>
                    <Grid item xs={12} sm={6} md={2}
                        container
                        direction="column"
                        alignItems="center"
                        justifyContent='center'>
                        <span className="---17-Personas">
                            Combo Fiesta
                        </span>
                        <span className="span-1">
                            <span className="text-style-1">$</span>{comboPrice}
                        </span>
                        <span className="Slo-pastel">
                            *Bebidas incluidas
                        </span>
                        <Button variant="outlined" color="primary" className={classes.mgt4}>
                            Seleccionar
                        </Button>
                    </Grid>
                </Grid>

            </div>

        </Grid>
    )
}