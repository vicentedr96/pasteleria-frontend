import React, { useEffect, useState } from 'react';
import { Grid, TextField, Select, makeStyles } from '../Utils/Constant/core';
import SelectField from '../Utils/Generic/SelectField';
import { SearchIcon } from "../Utils/Constant/icons";
import Card from "./Card";
import { Fetch } from "../Utils/Helpers/Fetch";

const useStyles = makeStyles(theme => ({
    alinearTextField: {
        marginTop: "10px", background: "white"
    },
    levantar: {
        padding: "10px" 
    },
     centrar: {
        [theme.breakpoints.down('sm')]: {
            padding: '10px 12px 30px',
        }
    },
    linea:{
        borderBottom:"1px solid #9f7fb1",margin:"10px",
        [theme.breakpoints.up('sm')]: {
            display:"none",
        }
    }
}));
let schema = {
    name: {
        valor: "",
        error: false
    },
    comboPrice: {
        valor: "",
        error: false
    },
    price: {
        valor: "",
        error: false
    },
    toppingType: {
        valor: "",
        error: false
    },
    ordenarPor: {
        valor: "ASC",
        error: false
    },
    size: {
        valor: "",
        error: false
    }
}

export default function Cuerpo() {
    const classes = useStyles();
    const [list, setList] = useState([]);
    const [getForm, setForm] = useState(schema);
    //EVENTOS
    const handleOnClickTextField = (e) => {
        let { name, value } = e.target;
        if (value === null || value.length === 0) {
            setForm((prevProps) => ({
                ...prevProps,
                [name]: {
                    valor: value,
                    error: true
                }
            }));
        } else {
            setForm((prevProps) => ({
                ...prevProps,
                [name]: {
                    valor: value,
                    error: false
                }
            }));
        }
    };
    const handleChangeSelect = (event) => {
        let { name, value: values } = event.target;
        setForm((prevProps) => ({
            ...prevProps,
            [name]: {
                valor: values || "",
                error: false
            }
        }));
    };

    useEffect(() => {
        const FetchData = async () => {
            try {
                let data = await Fetch("https://pasteleriavc-backend.herokuapp.com/productos/filtro", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        "name": getForm.name.valor,
                        "comboPrice": getForm.comboPrice.valor,
                        "price": getForm.price.valor,
                        "toppingType": (getForm.toppingType.valor).trim(),
                        "size": (getForm.size.valor).trim(),
                        "ordenarPor": getForm.ordenarPor.valor
                    }),
                });
                setList(data?.data || [])
            } catch (e) {
            }
        }
        FetchData()
    }, [getForm.toppingType.valor, getForm.size.valor,
    getForm.ordenarPor.valor, getForm.price.valor,
    getForm.comboPrice.valor, getForm.name.valor]);

    return (
        <Grid className="pd-20">
            <Grid
                className="contenedor-filtro-1"
                container
                direction="row"
                justifyContent="space-around"
            >
                <Grid item xs={12} sm={7} md={3} lg={2}>
                    <TextField
                        variant="outlined" margin="normal"
                        fullWidth
                        label="Nombre"
                        className={classes.alinearTextField}
                        InputProps={{
                            startAdornment: <SearchIcon />
                        }}
                        value={getForm?.name?.valor}
                        inputProps={{ onChange: handleOnClickTextField }}
                        name="name"
                    />
                </Grid>
                <Grid item xs={12} className={classes.linea}/>
                <Grid item xs={12} sm={4} md={3} lg={2} className={classes.levantar}>
                    <SelectField title={"Precio"}>
                        <Select
                            native
                            label="Precio"
                            name="price"
                            className="bgWhite"

                            onChange={handleChangeSelect}
                            value={getForm?.price?.valor || ""}
                            inputProps={{
                                name: 'price',
                                id: 'outlined-age-native-simple-paquete--02',
                            }}
                        >
                            <option value={"TODOS"}>Todos</option>
                            <option value={"ECONÓMICOS"}>Económicos</option>
                        </Select>
                    </SelectField>
                </Grid>
                <Grid item xs={12} sm={4} md={3} lg={2} className={classes.levantar}>
                    <SelectField title={"Precio combo"}>
                        <Select
                            native
                            label="Precio combo"
                            name="comboPrice"
                            className="bgWhite"
                            onChange={handleChangeSelect}
                            value={getForm?.comboPrice?.valor || ""}
                            inputProps={{
                                name: 'comboPrice',
                                id: 'outlined-age-native-simple-paquete--02',
                            }}
                        >
                            <option value={"TODOS"}>Todos</option>
                            <option value={"ECONÓMICOS"}>Económicos</option>
                        </Select>
                    </SelectField>
                </Grid>
                <Grid item xs={12} sm={4} md={2} lg={1} className={classes.levantar}>
                    <SelectField title={"Topping"}>
                        <Select
                            native
                            label="Topping"
                            name="toppingType"
                            className="bgWhite"
                            onChange={handleChangeSelect}
                            value={getForm?.toppingType?.valor || ""}
                            inputProps={{
                                name: 'toppingType',
                                id: 'outlined-age-native-simple-paquete--02',
                            }}
                        >   <option value={" "}>Todos</option>
                            <option value={"0"}>Fondeau</option>
                            <option value={"1"}>Betun Italiano</option>
                            <option value={"2"}>Chantilly</option>
                        </Select>
                    </SelectField>
                </Grid>
                <Grid item xs={12} sm={3} md={6} lg={1} className={classes.levantar}>
                    <SelectField title={"Tamaño"}>
                        <Select
                            native
                            label="Tamaño"
                            name="size"
                            className="bgWhite"
                            onChange={handleChangeSelect}
                            value={getForm?.size?.valor || ""}
                            inputProps={{
                                name: 'size',
                                id: 'outlined-age-native-simple-paquete--02',
                            }}
                        >   <option value={" "}>Todos</option>
                            <option value={"S"}>Pequeño</option>
                            <option value={"M"}>Mediano</option>
                            <option value={"L"}>Grande</option>
                        </Select>
                    </SelectField>
                </Grid>
                <Grid item xs={12} sm={12} md={5} lg={2} className={classes.levantar}>
                    <SelectField title={"Ordenar por"}>
                        <Select
                            native
                            label="Ordenar por"
                            name="ordenarPor"
                            className="bgWhite"
                            onChange={handleChangeSelect}
                            value={getForm?.ordenarPor?.valor || ""}
                            inputProps={{
                                name: 'ordenarPor',
                                id: 'outlined-age-native-simple-paquete--02',
                            }}
                        >
                            <option value={"ASC"}>Ascendente</option>
                            <option value={"DESC"}>Descedente</option>
                        </Select>
                    </SelectField>
                </Grid>

            </Grid>
            <div className={classes.centrar +" contenedor-filtro-2"}>
                {
                    list.map((data, index) => {
                        return (
                            <Card key={index} {...data} />
                        )
                    })
                }
            </div>

        </Grid>
    )
}
